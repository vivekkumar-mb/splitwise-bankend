const check_email_exists = (email, DB_emails) => {
  let value = false;
  DB_emails.forEach((element) => {
    if (element["email"] === email) {
      value = true;
    }
  });
  return value;
};

module.exports = check_email_exists;
