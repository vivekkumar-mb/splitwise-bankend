const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  id: Number,
  username: String,
  email: String,
  password: String,
  friends: [Number],
  groupIds: [Number],
});

module.exports = mongoose.model("User", userSchema);
