const mongoose = require("mongoose");

const groupSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  id: Number,
  groupName: String,
  groupMembers: [Number],
  expenses: [
    {
      expenseDesc: String,
      expenseAmount: Number,
      payer: String,
      unpaid: String,
      settlements: String,
    },
  ],
});

module.exports = mongoose.model("Group", groupSchema);
