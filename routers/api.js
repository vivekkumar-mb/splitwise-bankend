const express = require("express");
require("dotenv").config();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();
const mongoose = require("mongoose");
const User = require("../models/users");
const Group = require("../models/groups");
const { makeUnpaid, makeSettlements } = require("../helpme");
const check_email_exists = require("../helpers/email_helper");
mongoose.connect(
  `mongodb+srv://vivek-kumar:${process.env.DB_PASSWORD}@cluster0.y4kv9.mongodb.net/Splitwise?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
);
router.post("/user/register", async (req, res) => {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const friends = [];
  const groupIds = [];
  let newId = await User.findOne().sort({ id: -1 });
  if (newId === null) {
    newId = {};
    newId["id"] = 0;
  }
  const DB_emails = await User.find({}, { email: 1, _id: 0 });
  const duplicate_email = check_email_exists(email, DB_emails);
  if (!duplicate_email) {
    const salt = await bcrypt.genSalt(10);
    const encryptedPassword = await bcrypt.hash(password, salt);
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      id: newId["id"] + 1,
      username: username,
      email: email,
      password: encryptedPassword,
      friends: friends,
      groupIds: groupIds,
    });
    await newUser
      .save()
      .then((results) => {
        console.log(results);
        res.json({
          message: "ok",
        });
      })
      .catch((err) => {
        console.log(err);
        res.json({
          message: "failed to register",
        });
      });
  } else {
    res.json({
      message: "email already exists",
    });
  }
});
router.post("/user/login", async (req, res) => {
  console.log(req.body);
  const email = req.body.email;
  const password = req.body.password;
  const DB_emails = await User.find({}, { email: 1, _id: 0 });
  if (check_email_exists(email, DB_emails)) {
    const userID = await User.findOne({ email: email }, { id: 1, _id: 0 });
    const DB_Password = await User.findOne(
      { email: email },
      { password: 1, _id: 0 },
    );
    const password_Valid = await bcrypt.compare(
      password,
      DB_Password["password"],
    );
    if (password_Valid) {
      jwt.sign(
        { data: email },
        process.env.SECRET_KEY,
        { expiresIn: "1h" },
        (err, token) => {
          if (err) {
            console.log(err.message);
            res.json({
              message: "jwt failed",
            });
          } else {
            res.json({
              message: "token created",
              signature: token,
              email: email,
              userID: userID["id"],
            });
          }
        },
      );
    } else {
      res.json({
        message: "password does not match",
      });
    }
  } else {
    res.json({
      message: "email does not exists",
    });
  }
});

//get user info
router.get("/user/:userEmail", async (req, res) => {
  const email = req.params.userEmail;
  const userData = await User.findOne({ email: email });
  const sendData = { friends: [], groups: [] };
  for (let friendId of userData.friends) {
    const friendData = await User.findOne({ id: friendId });
    // console.log(friendData.username);
    sendData.friends.push([friendId, friendData.username]);
  }
  const expenses = [];
  for (let groupId of userData.groupIds) {
    const groupData = await Group.findOne({ id: groupId });
    console.log(groupData.groupName);
    groupData.expenses.forEach((expense) => {
      expenses.push(JSON.parse(expense.settlements));
    });
    sendData.groups.push([groupId, groupData.groupName]);
  }

  const finalSettlement = expenses.reduce((accumulator, element) => {
    if (element.hasOwnProperty(userData.id)) {
      for (const id in element[userData.id]) {
        if (accumulator[id] === undefined) {
          accumulator[id] = element[userData.id][id];
        } else {
          accumulator[id] += element[userData.id][id];
        }
      }
      return accumulator;
    } else {
      for (const id in element) {
        if (element[id].hasOwnProperty(userData.id)) {
          if (accumulator[id] === undefined) {
            accumulator[id] = -1 * element[id][userData.id];
          } else {
            accumulator[id] -= element[id][userData.id];
          }
        }
      }
      return accumulator;
    }
  }, {});
  console.log(finalSettlement);
  sendData["finalSettlement"] = finalSettlement;
  sendData["username"] = userData.username;
  res.json(sendData);
});
//get group info pass groupID
router.get("/user/group/:groupID", async (req, res) => {
  const groupID = req.params.groupID;
  const groupData = await Group.findOne({ id: groupID });
  if (groupData !== null)
    res.json({
      message: "got group data!",
      data: groupData,
    });
  else {
    res.json({
      message: "group does not exist!",
    });
  }
});
// get groupinfo of two friends
router.get("/user/friend/:userID/:friendID", async (req, res) => {
  const friendID = req.params.friendID;
  const userID = req.params.userID;
  const groupData1 = await Group.findOne({
    groupName: `${userID} and ${friendID}`,
  });
  const groupData2 = await Group.findOne({
    groupName: `${friendID} and ${userID}`,
  });
  if (groupData1 !== null) {
    res.json({
      message: "got friend data!!",
      data: groupData1,
    });
  } else if (groupData2 !== null) {
    res.json({
      message: "got friend data!!!!",
      data: groupData2,
    });
  } else if (groupData1 === null && groupData2 === null) {
    res.json({
      message: "data not present",
    });
  } else
    res.json({
      message: "failed",
    });
});

// add friend and user  in eachOther friends array(provided both friend and user are registered)
//post:userID and friendEmail
router.post("/user/addFriend", async (req, res) => {
  let b1 = false;
  let b2 = false;
  const userID = req.body.userID;
  const friendEmail = req.body.friendEmail;
  //console.log(friendEmail);
  const DB_emails = await User.find({}, { email: 1, _id: 0 });
  if (check_email_exists(friendEmail, DB_emails)) {
    const temp = await User.findOne({ email: friendEmail }, { id: 1, _id: 0 });
    const friendID = temp["id"];
    const userData = await User.findOne({ id: userID });
    const friendData = await User.findOne({ id: friendID });
    if (!userData["friends"].includes(friendID)) {
      userData["friends"].push(friendID);
      friendData["friends"].push(userID);
      A = await User.findOneAndUpdate(
        { id: userID },
        userData,
        { upsert: false },
        function (err, doc) {
          if (err) {
            console.log(err);
          } else {
            b1 = true;
            console.log("Succesfully saved.");
          }
        },
      ).then((data) => {
        b1 = true;
      });
      B = await User.findOneAndUpdate(
        { id: friendID },
        friendData,
        { upsert: false },
        function (err, doc) {
          if (err) {
            console.log(err);
          } else {
            b2 = true;
            console.log("Successfully saved.");
          }
        },
      ).then((data) => {
        b2 = true;
      });
      console.log(b1, b2);
      if (b1 && b2) {
        res.json({
          message: "Made Friend Successfully",
        });
      } else {
        res.json({
          message: "Friend Not Made, Database error!",
        });
      }
    } else {
      res.json({
        message: "Friend Already added!",
      });
    }
  } else {
    res.json({
      message: "Friend Not Registered!!",
    });
  }
});

// addExpense between two friends
//post:userID,friendID,expenseDesc,expenseAmount,payerArr,unpaidArr,settlementObj
router.post("/user/addExpense/friend", async (req, res) => {
  console.log(req.body);
  let groupData;
  const friendID = parseInt(req.body.friendID);
  const userID = parseInt(req.body.userID);
  const desc = req.body.expenseDesc;
  const descAmt = parseInt(req.body.expenseAmount);
  const tempArr = [];
  const userObj = {};
  userObj[userID] = descAmt;
  const friendObj = {};
  friendObj[friendID] = 0;
  tempArr.push(userObj);
  tempArr.push(friendObj);
  const tempArr2 = [];
  uObj = {};
  uObj[userID] = descAmt / 2;
  fObj = {};
  fObj[friendID] = -1 * (descAmt / 2);
  tempArr2.push(uObj);
  tempArr2.push(fObj);
  const payer = tempArr;
  const unpaid = tempArr2;
  const settlementObj = {};
  settlementObj[userID] = {};
  settlementObj[userID][friendID] = descAmt / 2;
  //console.log(payer, unpaid, settlementObj);
  const groupData1 = await Group.findOne({
    groupName: `${userID} and ${friendID}`,
  });
  const groupData2 = await Group.findOne({
    groupName: `${friendID} and ${userID}`,
  });

  groupData = groupData1 || groupData2;
  if (groupData === null) {
    let newId = await Group.findOne().sort({ id: -1 });
    if (newId === null) {
      newId = {};
      newId["id"] = 0;
    }
    const newGroup = new Group({
      _id: new mongoose.Types.ObjectId(),
      id: newId["id"] + 1,
      groupName: `${userID} and ${friendID}`,
      groupMembers: [userID, friendID],
      expenses: [
        {
          expenseDesc: desc,
          expenseAmount: descAmt,
          payer: JSON.stringify(payer),
          unpaid: JSON.stringify(unpaid),
          settlements: JSON.stringify(settlementObj),
        },
      ],
    });
    await newGroup
      .save()
      .then(async (results) => {
        // console.log(results);
        let newId = await Group.findOne().sort({ id: -1 });
        const userData = await User.findOne({ id: userID });
        const friendData = await User.findOne({ id: friendID });
        userData["groupIds"].push(newId["id"]);
        friendData["groupIds"].push(newId["id"]);
        await User.findOneAndUpdate(
          { id: userID },
          userData,
          { upsert: false },
          function (err, doc) {
            if (err) {
              console.log(err);
            } else {
              b1 = true;
              console.log("Succesfully saved.");
            }
          },
        );
        await User.findOneAndUpdate(
          { id: friendID },
          friendData,
          { upsert: false },
          function (err, doc) {
            if (err) {
              console.log(err);
            } else {
              b2 = true;
              console.log("Succesfully saved.");
            }
          },
        );
        res.json({
          message: "two user group created",
        });
      })
      .catch((err) => {
        console.log(err);
        res.json({
          message: "two user group create failed",
        });
      });
  } else {
    const temp = {
      _id: new mongoose.Types.ObjectId(),
      expenseDesc: desc,
      expenseAmount: descAmt,
      payer: JSON.stringify(payer),
      unpaid: JSON.stringify(unpaid),
      settlements: JSON.stringify(settlementObj),
    };
    groupData["expenses"].push(temp);
    if (groupData1) {
      await Group.findOneAndUpdate(
        {
          groupName: `${userID} and ${friendID}`,
        },
        groupData,
        { upsert: false },
        function (err, doc) {
          if (err) {
            console.log(err);
          } else {
            b1 = true;
            console.log("Succesfully saved.");
          }
        },
      );
      res.json({
        message: "expense updated",
      });
    }
    if (groupData2) {
      await Group.findOneAndUpdate(
        {
          groupName: `${friendID} and ${userID}`,
        },
        groupData,
        { upsert: false },
        function (err, doc) {
          if (err) {
            console.log(err);
          } else {
            b1 = true;
            console.log("Succesfully saved.");
          }
        },
      );
      res.json({
        message: "expense updated",
      });
    }

    //console.log(groupData["expenses"]);
  }
});

// this does two things
//1) create group of friends for that, post : membersArr,groupName
//2) addExpense in the group for that, post : groupID,expenseDesc,expenseAmount,payerArr,unpaidArr,settlementObj
router.post("/user/addExpense/group", async (req, res) => {
  console.log(req.body);
  const friends = req.body.membersArr;
  const groupName = req.body.groupName;
  //const groupID = JSON.parse(req.body.groupID);
  // const desc = req.body.expenseDesc;
  // const descAmt = req.body.expenseAmount;
  // const payer = JSON.parse(req.body.payerArr); // fix this maybe
  //console.log(friends, groupName);
  //const groupData = await Group.findOne({ id: groupID });
  const groupData = null;
  if (groupData === null) {
    let newId = await Group.findOne().sort({ id: -1 });
    if (newId === null) {
      newId = {};
      newId["id"] = 0;
    }
    const newGroup = new Group({
      _id: new mongoose.Types.ObjectId(),
      id: newId["id"] + 1,
      groupName: groupName,
      groupMembers: friends,
      expenses: [
        {
          expenseDesc: "",
          expenseAmount: 0,
          payer: "[]",
          unpaid: "[]",
          settlements: "{}",
        },
      ],
    });
    await newGroup
      .save()
      .then(async (results) => {
        //   //Updating group list of all member in group
        let newId = await Group.findOne().sort({ id: -1 });
        for (let user of friends) {
          const query = { id: user };
          const userData = await User.findOne(query);
          console.log(userData);
          if (!userData.groupIds.includes(newId["id"])) {
            userData.groupIds.push(newId["id"]);
          }
          await User.findOneAndUpdate(
            query,
            userData,
            { upsert: true },
            function (err, doc) {
              if (err) {
                console.log(err);
              } else {
                console.log("Succesfully saved.");
              }
            },
          ).then((res) => {
            console.log("user updated");
          });
        }
        res.json({
          message: "group created and expense added",
        });
      })
      .catch((err) => {
        console.log(err);
        res.json({
          message: "group create failed",
        });
      });
  } else {
    const unpaid = makeUnpaid(descAmt, payer);
    const settlementObj = makeSettlements(descAmt, payer);
    const temp = {
      _id: new mongoose.Types.ObjectId(),
      expenseDesc: desc,
      expenseAmount: descAmt,
      payer: payer,
      unpaid,
      unpaid,
      settlements: settlementObj,
    };
    groupData["expenses"].push(temp);
    await Group.findOneAndUpdate(
      { id: groupID },
      groupData,
      { upsert: false },
      function (err, doc) {
        if (err) {
          console.log(err);
        } else {
          b1 = true;
          console.log("Succesfully saved.");
          res.json({
            message: "expense added",
          });
        }
      },
    );
  }
});
//
router.get("/user/allExpenses/:userEmail", async (req, res) => {
  const email = req.params.userEmail;
  const userData = await User.findOne({ email: email });
  const userID = userData["id"];
  const userGroupArr = userData["groupIds"];
  if (!userGroupArr.length >= 1) {
    res.json({
      message: "user is not in any group",
    });
  }
  const expenses = {};
  for (let groupId of userGroupArr) {
    const groupData = await Group.findOne({ id: groupId });
    const expensesArr = groupData["expenses"];
    for (let eachEx of expensesArr) {
      const expenseDesc = eachEx["expenseDesc"];
      console.log();
      const payerArr = JSON.parse(eachEx["payer"]);

      for (let obj of payerArr) {
        const x = Object.entries(obj);
        if (JSON.parse(x[0][0]) === userID) {
          expenses[expenseDesc] = [x[0][1], eachEx["expenseAmount"]];
          //data:{biryani:[100,480]}
        }
      }
    }
  }
  res.send({
    message: "got expense",
    data: expenses,
  });
});
// async function insertGroup() {
//   let newId = await Group.findOne().sort({ id: -1 });
//   if (newId === null) {
//     newId = {};
//     newId["id"] = 0;
//   }
//   const newGroup = new Group({
//     _id: new mongoose.Types.ObjectId(),
//     id: newId["id"] + 1,
//     groupName: "Anonymous",
//     groupMembers: [1, 2],
//     expenses: [
//       {
//         expenseDesc: "biryani",
//         expenseAmount: 330,
//         payer: JSON.stringify([{ 1: 100 }, { 2: 230 }]),
//         unpaid: JSON.stringify([{ 1: -65 }, { 2: 65 }]),
//         settlements: JSON.stringify({ 2: { 1: 65 } }),
//       },
//       {
//         expenseDesc: "Rolls",
//         expenseAmount: 160,
//         payer: JSON.stringify([{ 1: 160 }, { 2: 0 }]),
//         unpaid: JSON.stringify([{ 1: 80 }, { 2: -80 }]),
//         settlements: JSON.stringify({ 1: { 2: 80 } }),
//       },
//       {
//         expenseDesc: "Rent",
//         expenseAmount: 12000,
//         payer: JSON.stringify([{ 1: 5000 }, { 2: 7000 }]),
//         unpaid: JSON.stringify([{ 1: -1000 }, { 2: 1000 }]),
//         settlements: JSON.stringify({ 2: { 1: 1000 } }),
//       },
//     ],
//   });
//   await newGroup
//     .save()
//     .then((results) => {
//       console.log(results);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
//   // const group1 = await Group.findOne({ id: 3 });
//   // console.log(JSON.parse(group1.expenses[0].payer));

//   //Updating group list of all member in group
//   for (let user of newGroup.groupMembers) {
//     const query = { id: user };
//     const userData = await User.findOne(query);
//     console.log(userData);
//     if (!userData.groupIds.includes(newGroup.id)) {
//       userData.groupIds.push(newGroup.id);
//     }
//     User.findOneAndUpdate(
//       query,
//       userData,
//       { upsert: true },
//       function (err, doc) {
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("Succesfully saved.");
//         }
//       },
//     );
//   }
//   //Updating friends list of all member in group
//   for (let index = 0; index < newGroup.groupMembers.length; index++) {
//     const query = { id: newGroup.groupMembers[index] };
//     const userData = await User.findOne(query);
//     console.log(userData);
//     for (let index2 = 0; index2 < newGroup.groupMembers.length; index2++) {
//       if (
//         index === index2 &&
//         !userData.friends.includes(newGroup.groupMembers[index2])
//       ) {
//         userData.friends.push(newGroup.groupMembers[index2]);
//       }
//     }
//     User.findOneAndUpdate(
//       query,
//       userData,
//       { upsert: true },
//       function (err, doc) {
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("Succesfully saved.");
//         }
//       },
//     );
//   }
// }
// async function insertGroup() {
//   let newId = await Group.findOne().sort({ id: -1 });
//   if (newId === null) {
//     newId = {};
//     newId["id"] = 0;
//   }
//   const newGroup = new Group({
//     _id: new mongoose.Types.ObjectId(),
//     id: newId["id"] + 1,
//     groupName: "Person",
//     groupMembers: [1, 2, 3, 4],
//     expenses: [
//       {
//         expenseDesc: "biryani",
//         expenseAmount: 480,
//         payer: JSON.stringify([{ 1: 100 }, { 2: 50 }, { 3: 140 }, { 4: 190 }]),
//         unpaid: JSON.stringify([{ 1: -20 }, { 2: -70 }, { 3: 20 }, { 4: 70 }]),
//         settlements: JSON.stringify({ 3: { 1: 20 }, 4: { 2: 70 } }),
//       },
//       {
//         expenseDesc: "Rolls",
//         expenseAmount: 320,
//         payer: JSON.stringify([{ 1: 160 }, { 2: 0 }, { 3: 60 }, { 4: 100 }]),
//         unpaid: JSON.stringify([{ 1: 80 }, { 2: -80 }, { 3: -20 }, { 4: 20 }]),
//         settlements: JSON.stringify({ 1: { 2: 80 }, 4: { 3: 20 } }),
//       },
//       {
//         expenseDesc: "Rent",
//         expenseAmount: 12000,
//         payer: JSON.stringify([
//           { 1: 5000 },
//           { 2: 5000 },
//           { 3: 1000 },
//           { 4: 1000 },
//         ]),
//         unpaid: JSON.stringify([
//           { 1: 2000 },
//           { 2: 2000 },
//           { 3: -2000 },
//           { 4: -2000 },
//         ]),
//         settlements: JSON.stringify({
//           1: { 3: 2000 },
//           2: { 4: 2000 },
//         }),
//       },
//     ],
//   });
//   await newGroup
//     .save()
//     .then((results) => {
//       console.log(results);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
//   // const group1 = await Group.findOne({ id: 3 });
//   // console.log(JSON.parse(group1.expenses[0].payer));

//   //Updating group list of all member in group
//   for (let user of newGroup.groupMembers) {
//     const query = { id: user };
//     const userData = await User.findOne(query);
//     console.log(userData);
//     if (!userData.groupIds.includes(newGroup.id)) {
//       userData.groupIds.push(newGroup.id);
//     }
//     User.findOneAndUpdate(
//       query,
//       userData,
//       { upsert: true },
//       function (err, doc) {
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("Succesfully saved.");
//         }
//       },
//     );
//   }
//   //Updating friends list of all member in group
//   for (let index = 0; index < newGroup.groupMembers.length; index++) {
//     const query = { id: newGroup.groupMembers[index] };
//     const userData = await User.findOne(query);
//     console.log(userData);
//     for (let index2 = 0; index2 < newGroup.groupMembers.length; index2++) {
//       if (
//         index !== index2 &&
//         !userData.friends.includes(newGroup.groupMembers[index2])
//       ) {
//         userData.friends.push(newGroup.groupMembers[index2]);
//       }
//     }
//     User.findOneAndUpdate(
//       query,
//       userData,
//       { upsert: true },
//       function (err, doc) {
//         if (err) {
//           console.log(err);
//         } else {
//           console.log("Succesfully saved.");
//         }
//       },
//     );
//   }
// }
// insertGroup();

module.exports = router;
